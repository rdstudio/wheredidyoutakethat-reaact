import { LOAD_USER } from '../actions/users/usersActionTypes'


const initialState = {}

const userReducer = (state = initialState, action) => {

    console.log(state)
    console.log(action)
    switch(action.type) {
        case LOAD_USER:
            return {
                ...state,
                current_user: action.payload
            }

        default:
            return state
    }
}

export default userReducer