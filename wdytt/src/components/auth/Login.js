import React, { Component } from 'react'
import axios from 'axios'
import { connect } from 'react-redux'
import { loadUser } from '../../actions/users/usersActionCreators'

class Login extends Component {

    submitForm = (e) => {
        axios.post('http://localhost:3000/api/v1/users/login', { user: { email: this.getEmail.value, password: this.getPassword.value } })
        .then(response => {
            console.log("User Logged In");
            console.log( response.data.user );
            // this.props.dispatch(loadUser(response.data))
            this.props.loadUser(response.data.user)
        })
        .catch( error => console.log(error))
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <h2>Login</h2>

                        <form className="form">
                            <div className="form-group">
                                <label htmlFor="email">Email Address</label>
                                <input type="email" id="email" className="form-control" ref={ (input) => this.getEmail = input } />
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Password</label>
                                <input type="password" id="password" className="form-control" ref={ (input) => this.getPassword = input } />
                            </div>
                            <div className="form-group">
                                <input type="button" className="btn btn-primary" value="Login" onClick={ () => this.submitForm() } />
                            </div>
                            <p>{ this.props.users } --- </p>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

// This can also be thought of as a select function since we are getting the user object from state
const mapStateToProps = (state) => {
    return {
        users: state.current_user
    }
}

const mapDispatchToProps = {
    loadUser
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)