import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import configureStore from './store/configureStore'
import { Provider } from 'react-redux'

// Load Component Pages for Naviation
import Header from './components/layout/Header'
import Home from './components/pages/Home'
import Login from './components/auth/Login'
import Register from './components/auth/Register'


function App() {
  return (
    <Provider store={configureStore}>
      <BrowserRouter>
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

configureStore.subscribe(App)

export default App;
