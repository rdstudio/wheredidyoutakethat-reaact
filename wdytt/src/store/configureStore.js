// This is the redux store file
import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'

/* Import All Reducers */
import usersReducer from '../reducers/usersReducer'

const rootReducer = combineReducers({
    users: usersReducer
})

const configureStore = createStore(
    rootReducer, 
    {
        users: []
    }, 
    composeWithDevTools(
        applyMiddleware(thunk)
    )
)

export default configureStore