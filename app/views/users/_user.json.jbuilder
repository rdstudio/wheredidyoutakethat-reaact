json.(user, :id, :email, :username, :bio, :image, :first_name, :last_name, :slug, :points)
json.token user.generate_jwt