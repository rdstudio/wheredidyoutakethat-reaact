class AddProfileFieldsToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :username, :string
    add_index :users, :username, unique: true
    add_column :users, :image, :string
    add_column :users, :bio, :text
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :slug, :string, null: false, default: ''
    add_column :users, :points, :integer, null: false, default: 0
  end
end
